# Build image
FROM node:10-alpine as build
ENV NODE_ENV='development'
WORKDIR /app
COPY package.json ./
COPY package-lock.json ./
COPY .npmrc ./
RUN npm install
COPY . .
RUN npm run build

# Actual server image
FROM node:10-alpine as prod
ENV TZ='Europe/Berlin' \
    NODE_ENV='production' \

WORKDIR /app

COPY package.json ./
COPY package-lock.json ./
COPY .npmrc ./
RUN npm install --ignore-scripts
COPY . .
RUN chmod +x scripts/* && \
    apk add --update --no-cache bash && \
    sed -i.bak "s/root:\/bin\/ash/root:\/bin\/bash/" /etc/passwd
COPY --from=build /app/dist ./dist

ENTRYPOINT ["scripts/docker-entrypoint.sh"]
CMD ["npm", "run", "start"]
