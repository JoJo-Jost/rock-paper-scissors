# Spielablauf
* Bewegungsphase
    * Creatures werden nach Bewegung gefragt
    * CreatureMovement wird ausgewertet
    * Kollisionen werden berechnet
* Kampfphase
    * Alle Kollidierenden Creatures werden nach ihrer "Kampfentscheidung" gefragt
    * Kämpfe werden ausgewertet
    * Ergebnisse werden in `FightHistory` geschrieben
* Ergebnisphase
    * Kampfergebnisse und TopScores werden an Creatures propagiert

# Open To-Do's



* Gameloop im `GameHandler` implementieren
    * Ruft `MovementHandler` mit allen registierten `creatures` auf (der die "Bewergungsphase" des Spiels handelt) 
    * Ruft `CollisioningHandler` auf (der eine List aller Kollidierenden Creatures zurückgibt)
    * Ruft `FightHandler` mit Liste aller Kollidierenden Creatures auf
    * Ruft `ScoreHandler` mit Liste aller Kollidierenden Creatures auf
    
* `MovementHandler` implementieren
    * bekomment Liste aller registrierten creatures als Argument übergeben
    * iteriert über registrierte `creatures` und fragt jede nach ihrer Bewegung
    * `GET` auf `/creature/move` 
        * sendet 406 wenn Movement nicht zulässig
        * updated im Erfolgsfall die Creature Position -> Aufruf von `CreatureStore.changeCreaturePosition()`
        * Was wenn > 2 creatures auf ein Feld wollen? wer darf hin?
* `FightHandler` implementieren
    * bekomment Liste aller kollidierenden creatures als Argument übergeben
    * Iteriert über Kollierdierende Creatures
            * POST /creature/fight
* `FightHistory` implementieren
    * 
    
